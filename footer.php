
<footer id="footer" class="top-space">

    <div class="footer1">
        <div class="container">
            <div class="row">

                <div class="col-md-3 widget">
                    <h3 class="widget-title">Contact</h3>
                    <div class="widget-body">
                        <p>+234 23 9873237<br>
                            <a href="mailto:#">some.email@somewhere.com</a><br>
                            <br>
                            36 rue de la falaise 78126 AULNAY-SUR-MAULDRE
                        </p>
                    </div>
                </div>

                <div class="col-md-3 widget">
                    <h3 class="widget-title">Suivez nous</h3>
                    <div class="widget-body">
                        <p class="follow-me-icons">
                            <a href=""><i class="fa fa-twitter fa-2"></i></a>
                            <a href=""><i class="fa fa-dribbble fa-2"></i></a>
                            <a href=""><i class="fa fa-github fa-2"></i></a>
                            <a href=""><i class="fa fa-facebook fa-2"></i></a>
                        </p>
                    </div>
                </div>

                <div class="col-md-6 widget">
                    <h3 class="widget-title">dimension 4 - Mentions Légales</h3>
                    <div class="widget-body">
                        <p>Tous les textes, commentaires, illustrations et images reproduits sur le site www.dim4.com sont protégés par le droit d’auteur et pour le monde entier. A ce titre et conformément aux dispositions du Code de la Propriété intellectuelle, seule l’utilisation pour un usage privé, réduite au cercle de famille, et la reproduction (impression, téléchargement) pour un usage strictement personnel sont autorisés. Toute autre utilisation est constitutive de contrefaçon et sanctionnée par le Code de la Propriété intellectuelle.</p>
                        <p>Les marques et logos figurant sur le site dim4.com sont la propriété exclusive de leurs détenteurs. Leur reproduction totale ou partielle est strictement interdite. Dimension 4 est une marque déposée appartenant à Dimension 4 SA.</p>
                    </div>
                </div>

            </div> <!-- /row of widgets -->
        </div>
    </div>

    <div class="footer2">
        <div class="container">
            <div class="row">

                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="simplenav">
                            <a href="#">Accueil</a> |
                            <a href="about.html">Individu</a> |
                            <a href="contact.html">Contact</a> |
                        </p>
                    </div>
                </div>

                <div class="col-md-6 widget">
                    <div class="widget-body">
                        <p class="text-right">
                            Copyright &copy; <?php echo date("Y")?>, Brice POTÉ. pour <a href="#" rel="designer">Dimension 4</a>
                        </p>
                    </div>
                </div>

            </div> <!-- /row of widgets -->
        </div>
    </div>

</footer>

<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="assets/js/headroom.min.js"></script>
<script src="assets/js/jQuery.headroom.min.js"></script>
<script src="assets/js/template.js"></script>
<script src="assets/js/myjavascript.js"></script>


</body>
</html>