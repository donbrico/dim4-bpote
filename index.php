<?php
require_once("./DbConnect.php");
require_once ("./header.php");
echo '<body class="home">';
include("./navbar.php");
$formAction = BASE_URL . "/liste.inc.php";
?>


    <!-- Header -->
	<header id="head">
		<div class="container">'
			<div class="row">
				<h1 class="lead"><?php echo $titreSite ?></h1>
				<p class="tagline">Rechercher d'un individu</p>
				<p>
                <form action="<?php echo $formAction ?>" class="form-inline" id="my_form" method="post">
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="nom" class="sr-only">Nom</label>
                        <input type="text"  class="btn mb-2" id="nom" name="nom" placeholder="Nom" style="width:500px!important;">
                    </div>
                    <br />
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="prenom" class="sr-only">Prénom</label>
                        <input type="text"  class="btn mb-2" id="prenom" name="prenom" placeholder="Prénom" style="width:500px!important;">
                    </div>
                    <br />
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="cp" class="sr-only">Code Postal</label>
                        <input type="text"  class="btn mb-2" id="cp" name="cp" placeholder="Code Postal" style="width:500px!important;">
                    </div>
                    <br />
                    <div class="form-group mx-sm-3 mb-2">
                        <label for="email" class="sr-only">Email</label>
                        <input type="text"  class="btn mb-2" id="email" name="email" placeholder="Email" style="width:500px!important;">
                    </div>
                    <br />
                    <input type="submit" class="btn btn-primary mb-2" value="Rechercher">
                </form>

                </p>
			</div>
		</div>
	</header>
	<!-- /Header -->

    <!-- Intro -->
    <div class="container text-center">
        <br> <br>
        <h2 class="thin">Les résultats de la recherche vont s'afficher dans cette partie</h2>
        <p class="text-muted">
            Cette page doit contenir un moteur de recherche permettant de trouver des individus de la base de données<br>
            les paramètres de recherche sont : le nom,le prénom,le code postale, l'email la liaison entre les paramètres est un ET.<br>


        </p>
    </div>
    <!-- /Intro-->

    <!-- container -->
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div id="server-results"><?php include ("./liste.inc.php")?></div>
            </div>
        </div> <!-- /row -->
    </div>	<!-- /container -->

<?php
require_once ("./footer.php");
?>