<?php
require_once ("./DbConnect.php");
$formAction = "./liste.inc.php";
require_once ("./header.php");
echo '<body class="home">';
include("./navbar.php");
?>

    <!-- Header -->
    <header id="head">
        <div class="container">'
            <div class="row">
                <h1 class="lead"><?php echo $titreSite ?></h1>
                <p class="tagline">&nbsp;</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </header>
    <!-- /Header -->

    <!-- Intro -->
    <div class="container text-center">
        <br> <br>
        <h2 class="thin">Gestion des individus</h2>
        <p>&nbsp;</p>
    </div>
    <!-- /Intro-->

    <!-- container -->
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div id="liste_full_individus"><?php include ("./listefull.inc.php")?></div>
            </div>
        </div> <!-- /row -->
    </div>	<!-- /container -->
    <div class="container text-center">
        <p><a class="btn btn-action btn-lg" role="button" href="./edit_user.php?id=0">Ajouter un individu</a></p>
    </div>

<?php
require_once ("./footer.php");
?>