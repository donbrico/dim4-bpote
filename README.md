Voici les paramètres de connexion

vhost :

ip : xx.xx.xx.xx
user : zzzzzz
password : wwwww

L'url du site est : http://www.xxxxxx.com/aaaaa/

php 5.3.19 est installé.

Base de donnée mysql 5.1:

ip : xx.xx.xx.xx
user : zzzzzz
password : wwwww
base  : wwwwwww

phpmyadmin n'est pas installé ni configuré sur le serveur. Vous pouvez utiliser n'importe quel client mysql pour vous connecter à cette base.

Le test :

Dans la base de données vous trouverez 3 tables

table Individu : comporte les coordonnées de 100 individus
les champs de la table sont :

id
id_civ
nom
prenom
adr1
adr2
adr3
adr4
cp
ville
id_pays
email

Les champs adr1 à adr4 correspondent à l'adresse postale de l'individu séparée en 4 lignes.
le champ cp est le code postal de l'adresse postale.
le champ ville est la ville de l'adresse postale.

id_civ fait référence à la table ref_civ
id_pays fait référence à la table ref_pays

table ref_civ : table de référence des civilités
table ref_pays : table de référence des pays

Voici le test :

Créer un petit site sur le vhost contenant 3 pages. L'apparence des pages nous importe peu.

page 1:

Cette page doit contenir un moteur de recherche permettant de trouver des individus de la base de données

les paramètres de recherche sont : le nom,le prénom,le code postale, l'email
la liaison entre les paramètres est un ET.

L'utilisateur doit pouvoir faire une recherche du style nom=BOSSU et email=jbossu@dim4.com

      L'internaute a la possibilité d'effectuer des filtres du type:
·         Commençant par
·         Contient
·         Finit par

Pour ce faire il doit utiliser le caractère "*".
Exemple de saisie dans le champ nom :
*CIMEN = tous les individus dont le nom finis par CIMEN (spécimen, st spécimen, jecimen,…)
spé* =  tous les individu dont le nom commence par spé (spécimen , SPECIALE , SPE QUELQUECHOSE,… )
*CI* = tous les individus dont le nom contient CI (spécimen, SPECIALE, Cigale, …).

Les résultats de la recherche apparaissent sous forme d'un tableau sous le moteur de recherche
Le tableau contient une ligne par résultat.

Dans chaque ligne est affiché les éléments suivants:

    l'id dfe l'enregistrement,
    le nom,
    le prénom,
    l'adresse postale 
    l'adresse email
    un bouton modifier qui envoi vers la page 2 du site
Sous le tableau de résultat se trouve un bouton AJOUTER qui envoi vers la page 3 du site.

Page 2 : (modification d'un individu)

Cette page comporte un formulaire permettant de modifier tous les champs (sauf id) de l'individu sélectionné sur la page 1.
Les champs sont pré remplis avec les données de la base.

Page 3 : (ajout d'un individu)

Cette page comporte un formulaire permettant d'ajouter un individu dans la base de données.
Tous les champs de la table individu doivent pouvoir être remplis (sauf id).

Dans les 2 formulaires les champs obligatoires sont civilité, nom, prénom, au moins une ligne adresse le code postal, ville et pays.

Si vous avez des questions ou le moindre souci de connexion ou de paramétrage, n'hésitez pas à revenir vers moi.
Comme précisé lors de notre entretien, nous attendons de votre part un email nous précisant vos délais pour réaliser ce site.

Je vous en remercie par avance,

Cordialement,