<?php
require("./Librairie.php");

try{
    $connection = new PDO('mysql:host=' . PDO_HOST . ';port=' . PDO_PORT . ';dbname=' . PDO_DBBASE . '', PDO_USER, PDO_PW);
}
catch (PDOException $e){
    print "Erreur de connexion à la BD !: " . $e->getMessage() . "<br/>";
    die();
}