<?php
$currentUrl = $_SERVER['REQUEST_URI'];
?>
<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
        <div class="navbar-header">
            <!-- Button for smallest screens -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="./index.php"><img src="assets/images/logo_v3.png" alt="<?php echo $titreSite ?>"></a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav pull-right">
                <li<?php echo (preg_match("#index#", $currentUrl)) ? ' class="active"':'' ?>><a href="./index.php">Accueil</a></li>
                <li<?php echo (preg_match("#edit_user#", $currentUrl) || (preg_match("#individu#", $currentUrl))) ? ' class="active"':'' ?>><a href="./individu.php">Individus</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<!-- /.navbar -->