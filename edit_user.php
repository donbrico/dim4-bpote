<?php
require_once("./DbConnect.php");
$data = $_GET;
$id = 0;
$add = true;
$bilanMessage = "";
require_once ("./DbConnect.php");

if(isset($_POST['traitement'])){

    if( trim($_POST['traitement']) == 1) { //UPD
        $add = false;

        $data = array(
            'id' => trim($_POST['id']),
            'id_civ' => trim($_POST['id_civ']),
            'nom' => trim($_POST['nom']),
            'prenom' => trim($_POST['prenom']),
            'adr1' => trim($_POST['adr1']),
            'adr2' => trim($_POST['adr2']),
            'adr3' => trim($_POST['adr3']),
            'adr4' => trim($_POST['adr4']),
            'cp' => trim($_POST['cp']),
            'ville' => trim($_POST['ville']),
            'id_pays' => trim($_POST['id_pays']),
            'email' => trim($_POST['email'])
        );

        $query = "UPDATE individu SET id_civ=:id_civ, nom=:nom, nom=:nom, prenom=:prenom, adr1=:adr1, adr2=:adr2, adr3=:adr3, adr4=:adr4, cp=:cp, ville=:ville, id_pays=:id_pays, email=:email WHERE id=:id";
    }

    if( trim($_POST['traitement']) == 2) { //ADD
        $add = true;

        $data = array(
            'id' => null,
            'id_civ' => trim($_POST['id_civ']),
            'nom' => trim($_POST['nom']),
            'prenom' => trim($_POST['prenom']),
            'adr1' => trim($_POST['adr1']),
            'adr2' => trim($_POST['adr2']),
            'adr3' => trim($_POST['adr3']),
            'adr4' => trim($_POST['adr4']),
            'cp' => trim($_POST['cp']),
            'ville' => trim($_POST['ville']),
            'id_pays' => trim($_POST['id_pays']),
            'email' => trim($_POST['email'])
        );

        $query = "INSERT INTO individu (id, id_civ, nom, prenom, adr1, adr2, adr3, adr4, cp, ville, id_pays, email) VALUES (:id, :id_civ, :nom, :prenom, :adr1, :adr2, :adr3, :adr4, :cp, :ville, :id_pays, :email)";
    }

    $r = $connection->prepare($query)->execute($data);

    if ($r) {
        $class='alert-success';
        $message = 'Traitement terminé avec succès !';
        if($add) $lastInsert = $connection->lastInsertId();
    } else {
       $class='alert-danger';
        $message = 'Un problème est survenu !';
    }
    $bilanMessage = '<div class="alert ' . $class . '" role="alert">' . $message . ' </div>';
    if(isset($lastInsert)) header("Location: ./edit_user.php?id=" . $lastInsert . "&success=1");
}



//$vide  = ( empty(array_filter($data, function($data, $key){ return $data != ""; },ARRAY_FILTER_USE_BOTH)) ) ? true : false;
//$vide  = ((trim($_POST['nom'])=='') && (trim($_POST['prenom'])=='') && (trim($_POST['cp'])=='') && (trim($_POST['email'])=='')) ? true : false;
function testSiVide(){
	if (isset($_POST['nom']) && (trim($_POST['nom'])=='') && (isset($_POST['prenom']) && trim($_POST['prenom'])=='') && (isset($_POST['cp']) && trim($_POST['cp'])=='') && (isset($_POST['email']) && trim($_POST['email'])=='')){
		return true;
	}else{
		return false;
	}
}

//$vide  = testSiVide();
$vide  = false;

if(!$vide && isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'];
}


$formAction = "./edit_user.php?id=" . $id;
require_once ("./header.php");
echo '<body class="home">';
include("./navbar.php");
if($id != 0) {
    $add = false;
    $query = "SELECT t1.*, t2.libelle as libelle_civ, t3.libelle as libelle_pays
                FROM individu as t1
                JOIN ref_civ as t2 on t1.id_civ = t2.id
                JOIN ref_pays as t3 on t1.id_pays = t3.id ";
    $query .= " WHERE t1.id = " . $id;
    $query .= " LIMIT 1;";
	
	//print_r($query);exit;

    $r = $connection->query($query);

    if (!empty($r)) {
        $result = $r->fetchAll(PDO::FETCH_ASSOC);
    } else {
        echo 'Aucune réponse trouvée, veuillez modifier vos critères de recherches !';
    }
    $titreForm = "Editer";
}else{
    $titreForm = "Ajouter";
}
$listeCivilite = $listePays = array();
$queryCivilite = "SELECT * FROM ref_civ;";

$rCivilite = $connection->query($queryCivilite);
if (!empty($rCivilite)) {
    $listeCivilite = $rCivilite->fetchAll(PDO::FETCH_ASSOC);
}
$queryPays = "SELECT * FROM ref_pays;";

$rPays = $connection->query($queryPays);
if (!empty($rPays)) {
    $listePays = $rPays->fetchAll(PDO::FETCH_ASSOC);
}
?>

    <!-- Intro -->
    <header id="head" class="secondary"></header>
    <div class="container text-center">
        <br> <br>
        <h2 class="thin"><?php echo $titreForm . " un individu"; ?></h2>
        <p><div id="bilanTraitement"></div></p>
    </div>

    <!-- /Intro-->

    <!-- container -->
    <div class="container">

        <div class="row">
            <!-- Article main content -->
            <article class="col-sm-9 maincontent">

                <br>
                <form method="post" action="<?php echo $formAction?>">
                    <input type="hidden" name="traitement" value="<?php echo ($add) ? '2':'1'?>>">
                    <input type="hidden" name="id" value="<?php echo (!$add) ? $result[0]['id']:""?>">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="custom-control custom-radio">
                                <?php
                                for($i=0;$i<count($listeCivilite);$i++){
                                ?>
                                    <input type="radio" class="custom-control-input" id="id_civ[]" name="id_civ" value="<?php echo $listeCivilite[$i]['id']?>" required="required" <?php echo (!$add && $result[0]['id_civ'] == $listeCivilite[$i]['id'] && !$add) ? ' checked':'' ?>
                                    <label class="custom-control-label" for="defaultUnchecked"><?php echo $listeCivilite[$i]['libelle']?></label>&nbsp;&nbsp;
                                    <?php
                                }
                                ?>
                            </div>
                            <br>
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Nom" name="nom" value="<?php echo (!$add) ? $result[0]['nom']:"" ?>"  required="required">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Prénom" name="prenom" value="<?php echo (!$add) ? $result[0]['prenom']:"" ?>" required="required">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Email" name="email" value="<?php echo (!$add) ? $result[0]['email']:"" ?>">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <input class="form-control" type="text" placeholder="Adresse 1" name="adr1" value="<?php echo (!$add) ? $result[0]['adr1']:'' ?>" required="required"><br>
                            <input class="form-control" type="text" placeholder="Adresse 2" name="adr2" value="<?php echo (!$add) ? $result[0]['adr2']:'' ?>"><br>
                            <input class="form-control" type="text" placeholder="Adresse 3" name="adr3" value="<?php echo (!$add) ? $result[0]['adr3']:'' ?>"><br>
                            <input class="form-control" type="text" placeholder="Adresse 4" name="adr4" value="<?php echo (!$add) ? $result[0]['adr4']:'' ?>"><br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Code postal" name="cp" value="<?php echo (!$add) ? $result[0]['cp']:"" ?>" required="required">
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" type="text" placeholder="Ville" name="ville" value="<?php echo (!$add) ? $result[0]['ville']:"" ?>" required="required">
                        </div>
                        <div class="col-sm-4">
                            <select class="form-control" id="id_pays" name="id_pays" required="required">
                            <?php
                            for($i=0;$i<count($listePays);$i++){
                                ?>
                                    <option value="<?php echo $listePays[$i]['id']?>" <?php echo (!$add && $result[0]['id_pays'] == $listePays[$i]['id']) ? ' selected':'' ?>><?php echo utf8_encode($listePays[$i]['libelle'])?></option>
                                <?php
                                }
                            ?>
                            </select>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <p>*Champs obligatoires</p>
                        </div>
                        <div class="col-sm-8 text-right">
                            <input class="btn btn-action" type="submit" value="Valider">
                        </div>

                    </div>
                </form>

            </article>
            <!-- /Article -->

        </div> <!-- /row -->
    </div>	<!-- /container -->

<?php

if(isset($_POST['traitement']) && ((int) $_POST['traitement'] >= 1)){
    echo "<script>
            $('#bilanTraitement').delay(300).fadeIn(function(){
                $('#bilanTraitement').append('" . $bilanMessage . "');
            });
            $('#bilanTraitement').delay(3000).fadeOut();
           </script>";
}
require_once ("./footer.php");
?>