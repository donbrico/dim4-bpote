<?php
require_once ("./DbConnect.php");
function completeQuery($monChamp, $saValeur){
    $regexBase = "#^\*|\*$#"; //on verifie les * dans la chaine au debut ou a la fin
    $query = "";
    if(preg_match($regexBase, $saValeur)){
        $query .= " AND t1." . $monChamp . " LIKE '" . preg_replace($regexBase, '%', $saValeur) . "'";
    }else{
        $query .= " AND t1." . $monChamp . "='" . $saValeur . "'";
    }
    return $query;
}

function limitText($string, $taille){
    // strip tags to avoid breaking any html
    $string = strip_tags($string);
    if (strlen($string) > $taille) {

        // truncate string
        $stringCut = substr($string, 0, $taille);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...';
    }
    echo $string;
}

function testSiVide(){
	if ((trim($_POST['nom'])=='') && (trim($_POST['prenom'])=='') && (trim($_POST['cp'])=='') && (trim($_POST['email'])=='')){
		return true;
	}else{
		return false;
	}
}

//Petite fonction pour tester le POST vide car empty et count ne marchent pas super
$data = $_POST;
//$vide  = ( empty(array_filter($data, function($data, $key){ return $data != ""; },ARRAY_FILTER_USE_BOTH)) ) ? true : false;
$vide  = testSiVide();


//Traitement des réponses :
if($vide){
    echo "<div class=\"alert alert-danger\" role=\"alert\">
              Aucune donnée disponible, merci d'affiner votre recheche.
            </div>";
} else {
        $nom = addslashes(htmlspecialchars(trim($data['nom'])));
        $prenom = addslashes(htmlspecialchars(trim($data['prenom'])));
        $cp = addslashes(htmlspecialchars(trim($data['cp'])));
        $email = addslashes(htmlspecialchars(trim($data['email'])));

        $result = array();
        $query = "SELECT t1.*, t2.libelle as libelle_civ, t3.libelle as libelle_pays
                FROM individu as t1
                JOIN ref_civ as t2 on t1.id_civ = t2.id
                JOIN ref_pays as t3 on t1.id_pays = t3.id ";
        $query .= " WHERE 1 = 1";
        if($nom != ''){
            $query .= completeQuery('nom', $nom);
        }
        if($prenom != ''){
            $query .= completeQuery('prenom', $prenom);
        }
        if($cp != ''){
            $query .= completeQuery('cp', $cp);
        }
        if($email != ''){
            $query .= completeQuery('email', $email);
        }

        $query .= ";";
        //print_r($query);exit;
        $r = $connection->query($query);

        if (!empty($r)) {
            $result = $r->fetchAll(PDO::FETCH_ASSOC);
        } else {
            echo 'Aucune réponse trouvée, veuillez modifier vos critères de recherches !';
        }
        $titreTableau = "Résultats de votre recherche";
        ?>
        <table id="liste_individu" class="display compact" style="width:100%">
            <caption>
                <h4><?php echo $titreTableau ?></h4>
            </caption>

            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Adresse postale</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $count = count($result);
                for ($i = 0; $i < $count; $i++) {
                    ?>
                    <tr>
                        <td><a href="./edit_user.php?id=<?php echo $result[$i]['id'] ?>"><?php echo $result[$i]['id'] ?></a>
                        </td>
                        <td><?php echo $result[$i]['nom'] ?></td>
                        <td><?php echo $result[$i]['prenom'] ?></td>
                        <td><?php echo limitText($result[$i]['adr1'] . '<br>' . $result[$i]['adr2'] . '<br>' . $result[$i]['adr3'] . '<br>' . $result[$i]['adr4'], 50) . '<br>' . $result[$i]['cp'] . '<br>' . $result[$i]['ville'] ?></td>
                        <td><?php echo $result[$i]['email'] ?></td>

                    </tr>
                    <?php
                }
                ?>
                </tbody>
        </table>
        <?php
}
?>
<script>
    $(document).ready(function(){
        $('#liste_individu').DataTable({
            "searching": false,
            "paging" : false
        });
    });
</script>

