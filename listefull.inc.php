<?php
require_once ("./DbConnect.php");
//Traitement des réponses :
    $result = array();
    $query = "SELECT t1.*, t2.libelle as libelle_civ, t3.libelle as libelle_pays
                FROM individu as t1
                JOIN ref_civ as t2 on t1.id_civ = t2.id
                JOIN ref_pays as t3 on t1.id_pays = t3.id ";
    $query .= " WHERE 1 = 1";
    $query .= ";";
    
    $r = $connection->query($query);

    if (!empty($r)) {
        $result = $r->fetchAll(PDO::FETCH_ASSOC);
    } else {
        echo 'Aucune réponse trouvée, veuillez modifier vos critères de recherches !';
    }
    $titreTableau = "Liste des individus";
	//print_r($result);exit;
    ?>
    <table id="liste_full_individu" class="display compact" style="width:100%">
        <caption>
            <h4><?php echo $titreTableau ?></h4>
        </caption>

        <thead>
            <tr>
                <th>ID</th>
                <th>Civilité</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th>CP</th>
                <th>Ville</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $count = count($result);
            for ($i = 0; $i < $count; $i++) {
                ?>
                <tr>
                    <td><a href="./edit_user.php?id=<?php echo $result[$i]['id'] ?>"><?php echo $result[$i]['id'] ?></a>
                    </td>
                    <td><?php echo $result[$i]['libelle_civ'] ?></td>
                    <td><?php echo ($result[$i]['nom']) ?></td>
                    <td><?php echo ($result[$i]['prenom']) ?></td>
                    <td><?php echo $result[$i]['cp'] ?></td>
                    <td><?php echo ($result[$i]['ville']) ?></td>

                </tr>
                <?php
            }
            ?>
            </tbody>
    </table>
<script>
    $(document).ready(function(){
        $('#liste_full_individu').DataTable({
            "searching": false,
            "paging" : true
        });
    });
</script>

